# Google Landmark Recognition Challenge #
This repository contains the code for the Google Landmark Recognition Challenge in Kaggle. This was done as a part of "CS5525: Data Analytics" course.

### Dependency
    1. Tensorflow
    2. Tensorflow-slim

### Setup
Here is the instruction for setting up the dataset for training with pretrained models. I am using [tensorflow-slim](https://github.com/tensorflow/models/tree/master/research/slim/) for training. 

```
# First Downlaod from the kaggle site: https://www.kaggle.com/c/landmark-recognition-challenge
# These only contains the csv file with links to the URLS
# Use the script from Kaggle to download those files
# Using 50 threads it takes around 7-8hrs to download the files
# generate tfrecord for the slim input
python src/data_convert.py 


# set path for training
DATASET_DIR=/path/to/output
TRAIN_DIR=/path/to/train/inception_v4
CHECKPOINT_PATH=/path/to/checkpoints/inception_v4.ckpt
# go to slim directory
# retrain only last layer
# copy src/landmark.py to the slim/datasets directory
python train_image_classifier.py \
    --train_dir=${TRAIN_DIR} \
    --dataset_dir=${DATASET_DIR} \
    --dataset_name=landmark \
    --dataset_split_name=validation \
    --model_name=inception_v4 \
    --checkpoint_path=${CHECKPOINT_PATH} \
    --checkpoint_exclude_scopes=inception_v4/logits \
    --max_number_of_steps=20000 \
    --batch_size=64 \
    &>out_inception_v4.log &


# evaluation
python eval_image_classifier.py \
    --alsologtostderr \
    --checkpoint_path=${TRAIN_DIR} \
    --dataset_dir=${DATASET_DIR} \
    --dataset_name=landmark \
    --dataset_split_name=validation \
    --model_name=inception_v4

# come back to this directory
# set input path
INFILE=/home/emon/kaggle/landmark-recognition-challenge/repo/cs5525-google_landmark_recognition_challenge/data/csv/test.csv
IMAGE_BASE=/home/emon/kaggle/landmark-recognition-challenge/repo/cs5525-google_landmark_recognition_challenge/data/test/
# predict classes
python src/predict.py \
    --checkpoint_path=${TRAIN_DIR} \
    --infile=${INFILE} \
    --model_name=inception_v4 \
    --outfile=inception_v4_pred.log \
    --image_base=${IMAGE_BASE}

```


### Contact 
contact me: momen@vt.edu